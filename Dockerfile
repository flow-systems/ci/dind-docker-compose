FROM docker:dind

RUN wget "https://github.com/docker/compose/releases/download/1.28.2/docker-compose-$(uname -s)-$(uname -m)" -O /usr/local/bin/docker-compose
RUN chmod +x /usr/local/bin/docker-compose

RUN docker-compose -v
